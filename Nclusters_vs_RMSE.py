import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
import math
import winsound
from scipy.stats import logistic
from sklearn import linear_model
from sklearn.metrics import r2_score
from sklearn.cluster import AgglomerativeClustering
import math

def price_per_sqft(row):
  return 1.0 * row["price"] / row["sqft_living"]

def squaredDif(x1, x2):
  return (x1 - x2) * (x1 - x2)

house = pd.read_csv("kc_house_data.csv")
features = ['long', 'lat']
X = house[features]

train_size_total = 9 * len(house.index) / 10
test_size_total = len(house.index) - train_size_total

for n_clusters in range(1, 100):

  cluster_labels = AgglomerativeClustering(n_clusters=n_clusters).fit_predict(X)
  house['cluster'] = pd.Series(cluster_labels, index=house.index)
  SE_train = 0
  SE_test = 0
  for i in range(0,n_clusters):
    cluster_house = house[house.cluster == i].copy()

    size = len(cluster_house.index)
    train_size = 9 * size / 10
    test_size = size - train_size

    cluster_house_train = cluster_house.head(train_size).copy() #90%
    X_train = cluster_house_train[["bathrooms", "sqft_living", "grade"]]
    y_train = cluster_house_train["price"]
    regr = linear_model.LinearRegression()
    regr.fit(X_train, y_train)
    y_pred_train = regr.predict(X_train)

    # remove outliers
    indeces = sorted(range(0, train_size), key=lambda i: squaredDif(y_train.iloc[i], y_pred_train[i]))
    indeces = indeces[0 : 9 * train_size / 10]
    X_train_no_outlier = X_train.iloc[indeces]
    y_train_no_outlier = y_train.iloc[indeces]
    regr = linear_model.LinearRegression()
    regr.fit(X_train_no_outlier, y_train_no_outlier)
    

    cluster_house_test = cluster_house.tail(test_size).copy() # 10%
    X_test = cluster_house_test[["bathrooms", "sqft_living", "grade"]]
    y_test = cluster_house_test["price"]  

    y_pred_train = regr.predict(X_train)
    SE_train += sum(map(lambda i: squaredDif(y_train.iloc[i], y_pred_train[i]), range(0, train_size)))

    y_pred_test = regr.predict(X_test)
    SE_test += sum(map(lambda i: squaredDif(y_test.iloc[i], y_pred_test[i]), range(0, test_size)))

  print "N = %i: " % n_clusters + "RMSE_train = %.2f, " % math.sqrt(SE_train / train_size_total) + "RMSE_test = %.2f" % math.sqrt(SE_test / test_size_total)
  
winsound.Beep(1000,500)
