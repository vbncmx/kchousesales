import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
import math
import winsound
from scipy.stats import logistic
from mpl_toolkits.mplot3d import Axes3D

def getDistance(lon1, lat1, lon2, lat2):
    R = 6378137 #m    
    # https://stackoverflow.com/a/11172685/6483508
    dLon = (lon2 - lon1) * math.pi / 180
    dLat = (lat2 - lat1) * math.pi / 180
    a = math.sin(dLat/2) * math.sin(dLat/2) + math.cos(lat1 * math.pi / 180) * math.cos(lat2 * math.pi / 180) * math.sin(dLon/2) * math.sin(dLon/2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    return R * c

def getYear(row):
  return int(row["date"][:4])

def price_per_sqft(row):
  return 1.0 * row["price"] / row["sqft_living"]

house = pd.read_csv("kc_house_data.csv")
house["year"] = house.apply(lambda row: getYear(row), axis=1)
house = house.query('2005<yr_built<2017')

print len(house)

house["price_per_sqft"] = house.apply(lambda row: price_per_sqft(row), axis=1)

distances = house.apply(lambda row: getDistance(-122.22, 47.63, row["long"], row["lat"]), axis=1)

plt.scatter(house["lat"], house["price_per_sqft"], s=1, c='r')

winsound.Beep(1000,500)

plt.show()