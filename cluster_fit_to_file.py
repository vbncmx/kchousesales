import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
import math
import winsound
from scipy.stats import logistic
from sklearn import linear_model
from sklearn.metrics import r2_score
from sklearn.metrics import mean_squared_error
import math

def price_per_sqft(row):
  return 1.0 * row["price"] / row["sqft_living"]

def squaredDif(x1, x2):
  return (x1 - x2) * (x1 - x2)

house = pd.read_csv("ks_house_clustered_40.csv")

for i in range(0,40):  

  cluster_house = house[house.cluster == i].copy()

  size = len(cluster_house.index)
  train_size = 9 * size / 10
  test_size = size - train_size

  cluster_house_train = cluster_house.head(train_size).copy() #90%
  X_train = cluster_house_train[["bathrooms", "sqft_living", "grade"]]
  y_train = cluster_house_train["price"]
  regr = linear_model.LinearRegression()
  regr.fit(X_train, y_train)
  y_pred_train = regr.predict(X_train)

  # remove outliers
  indeces = sorted(range(0, len(X_train.index)), key=lambda i: (y_train.iloc[i] - y_pred_train[i]) * (y_train.iloc[i] - y_pred_train[i]))
  indeces = indeces[0 : 9 * len(indeces) / 10]
  X_train = X_train.iloc[indeces]
  y_train = y_train.iloc[indeces]
  regr = linear_model.LinearRegression()
  regr.fit(X_train, y_train)
  y_pred_train = regr.predict(X_train)

  cluster_house_test = cluster_house.tail(test_size).copy() # 10%
  X_test = cluster_house_test[["bathrooms", "sqft_living", "grade"]]
  y_test = cluster_house_test["price"]  

  y_pred_train = regr.predict(X_train)
  r2_train = r2_score(y_train, y_pred_train)
  RMSE_train = math.sqrt(mean_squared_error(y_train, y_pred_train))

  y_pred_test = regr.predict(X_test)
  r2_test = r2_score(y_test, y_pred_test)
  RMSE_test = math.sqrt(mean_squared_error(y_test, y_pred_test))

  print "cluster %i " % i + "(%i): " % size + "r2_train = %.2f, " % r2_train + "r2_test = %.2f, " % r2_test + "RMSE_train = %.2f, " % RMSE_train + "RMSE_test = %.2f" % RMSE_test
  
winsound.Beep(1000,500)
