import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
import math
import winsound
from scipy.stats import logistic
from sklearn.cluster import AgglomerativeClustering
from sklearn.preprocessing import MinMaxScaler
import random


kelly_colors = ['#F2F3F4', '#222222', '#F3C300', '#875692', '#F38400', '#A1CAF1', '#BE0032', '#C2B280', '#848482', '#008856', '#E68FAC', '#0067A5', '#F99379', '#604E97', '#F6A600', '#B3446C', '#DCD300', '#882D17', '#8DB600', '#654522', '#E25822', '#2B3D26']
random.shuffle(kelly_colors)


for N in range(1, 11):
    house = pd.read_csv("clustered_csv/kc_clustered_%i.csv" % N)
    
    plt.clf()
    cluster_labels = house.apply(lambda row: kelly_colors[row["dominate_cluster"]], axis=1)
    
    background = plt.imread("google_image.png")

    img_height = len(background)
    img_width = len(background[0])
    
    plt.imshow(np.flipud(background), origin='lower')
    plt.axis('off')

    long_min = house["long"].min()
    long_max = house["long"].max()
    lat_min = house["lat"].min()
    lat_max = house["lat"].max()

    x = house.apply(lambda row: img_width * (row["long"] - long_min) / (long_max - long_min), axis=1)
    y = house.apply(lambda row: img_height * (row["lat"] - lat_min) / (lat_max - lat_min), axis=1)
    plt.scatter(x, y, s=1, c=cluster_labels, alpha=0.5)
    plt.xlim(0, img_width)
    plt.ylim(0, img_height)
    plt.title("N=%i" % N)
    plt.savefig("cluster_images/dominate/N%i.png" % N, bbox_inches='tight')
    # plt.savefig("cluster_images/non_dominate/N%i.png" % N)