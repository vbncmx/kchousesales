import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
import math
import winsound
from scipy.stats import logistic
from sklearn.cluster import AgglomerativeClustering
from sklearn.preprocessing import MinMaxScaler
import time

def getDistance(lon1, lat1, lon2, lat2):
    R = 6378137 #m    
    # https://stackoverflow.com/a/11172685/6483508
    dLon = (lon2 - lon1) * math.pi / 180
    dLat = (lat2 - lat1) * math.pi / 180
    a = math.sin(dLat/2) * math.sin(dLat/2) + math.cos(lat1 * math.pi / 180) * math.cos(lat2 * math.pi / 180) * math.sin(dLon/2) * math.sin(dLon/2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    return R * c

def getDominateCluster(lon, lat, house, cluster_amount):    
    # start = time.time()
    
    cluster_count = [0] * cluster_amount
    close_house = house[(abs(house.long - lon) < 0.01204) & (abs(house.lat - lat) < 0.006217)]

    for index, row in close_house.iterrows():        
        cluster_count[row["cluster"] - 1] += 1    
    dominate_cluster = cluster_count.index(max(cluster_count)) + 1
    
    # end = time.time()
    # print(end - start)

    return dominate_cluster

for N in range(5, 20):

    house = pd.read_csv("kc_house_data.csv")
    
    print((house["lat"].max() - house["lat"].min()) / 100)
    print((house["long"].max() - house["long"].min()) / 100)

    house["price_per_sqft"] = house.apply(lambda row: row["price"] / row["sqft_living"], axis=1)

    scaler = MinMaxScaler()
    features = ['long', 'lat', "price_per_sqft"]
    X = house[features]

    X = scaler.fit_transform(X)

    cluster_labels = AgglomerativeClustering(n_clusters=N).fit_predict(X)

    house['cluster'] = pd.Series(cluster_labels, index=house.index)

    house['dominate_cluster'] = house.apply(lambda row: getDominateCluster(row["long"], row["lat"], house, N), axis=1)

    # print("std=%.2f" % np.std(house["price_per_sqft"]))
    # for i in range(0, N):
    #     house_clustered = house[house.cluster == i].copy()
    #     std = np.std(house_clustered["price_per_sqft"])
    #     mean = np.mean(house_clustered["price_per_sqft"])
    #     print("N%i(%i): std=%.2f, mean=%.2f, std/mean=%.2f" % (i, len(house_clustered.index), std, mean, std / mean))    

    house.to_csv('clustered_csv/kc_clustered_%i.csv' % N, index=False)

winsound.Beep(1000,500)