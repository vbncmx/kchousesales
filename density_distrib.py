import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
import math
import winsound

def getDistance(lon1, lat1, lon2, lat2):
    R = 6378137 #m    
    # https://stackoverflow.com/a/11172685/6483508
    dLon = (lon2 - lon1) * math.pi / 180
    dLat = (lat2 - lat1) * math.pi / 180
    a = math.sin(dLat/2) * math.sin(dLat/2) + math.cos(lat1 * math.pi / 180) * math.cos(lat2 * math.pi / 180) * math.sin(dLon/2) * math.sin(dLon/2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    return R * c

house = pd.read_csv("ks_house_data_with_density.csv")
house['price_per_sqft'] = house.apply(lambda row: row["price"] / row["sqft_living"], axis=1)


plt.hist(house["density"], 60)

winsound.Beep(1000,500) 

plt.show()