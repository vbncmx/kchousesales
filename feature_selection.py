import pandas as pd
import numpy
import winsound
from sklearn.ensemble import RandomForestRegressor, ExtraTreesClassifier
from sklearn.model_selection import cross_val_score
from sklearn.metrics import r2_score
import math
from matplotlib import pyplot as plt

house = pd.read_csv("ks_house_clustered_5.csv")
house["date_in_days"] = house.apply(lambda row: int(row["date"][:4]) * 365, axis=1)


for cl in range(0,5):
	clustered_house = house[house.cluster==cl].copy()

	features = ["long", "lat", "grade", "bedrooms", "bathrooms", "sqft_living", "sqft_lot", "waterfront", "density", "sqft_above", "zipcode", "date_in_days"]
	X = numpy.array(clustered_house[features])
	Y = numpy.array(clustered_house["price"])

	regr = ExtraTreesClassifier(max_depth=10)
	regr.fit(X, Y)

	indeces = sorted(range(0, len(features)), key=lambda i: regr.feature_importances_[i], reverse=True)

	output = "N%i: " % cl
	for i in indeces:
		output += "%s=%.2f, " % (features[i], regr.feature_importances_[i])
	print(output)
