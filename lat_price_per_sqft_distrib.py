import pandas as pd
import winsound
from matplotlib import pyplot as plt 

house = pd.read_csv("ks_house_clustered_40.csv")

house["price_per_sqft"] = house.apply(lambda row: row["price"] / row["sqft_living"], axis=1)

house=house[house.cluster==2].copy()

plt.scatter(house["long"], house["price_per_sqft"], s=1, c='r')

winsound.Beep(1000,500) 

plt.show()
