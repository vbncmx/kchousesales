import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
import math
import winsound
from scipy.stats import logistic

def price_per_sqft(row):
  return 1.0 * row["price"] / row["sqft_living"]

house = pd.read_csv("ks_house_data_with_density.csv")

max_val = house["density"].max()

colors = house.apply(lambda row: (1.0 * row["density"] / max_val, 0, 0), axis=1)

plt.scatter(house["long"], house["lat"], 1, c=colors)

# plt.hist(sqft_prices, 100)

latMax = house["lat"].max()
latMin = house["lat"].min()
longMax = house["long"].max()
longMin = house["long"].min()

plt.xlim(longMin, longMax)
plt.ylim(latMin, latMax)

print "longMin=%.2f " % longMin + "latMin=%.2f " % latMin + "longMax=%.2f " % longMax + "latMax=%.2f " % latMax 

plt.savefig('long_lat_distrib_density.png', transparent=True)

winsound.Beep(1000,500)

# plt.show()