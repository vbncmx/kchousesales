import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
import math
import winsound
from scipy.stats import logistic

def price_per_sqft(row):
  return 1.0 * row["price"] / row["sqft_living"]

house = pd.read_csv("ks_house_clustered.csv")

for i in range(0,20):

  plt.clf()

  cluster_house = house[house.cluster == i].copy()

  cluster_house['year'] = cluster_house.apply(lambda row: row["date"][:4], axis=1)

  cluster_house["price_per_sqft"] = cluster_house.apply(lambda row: price_per_sqft(row), axis=1)

  cluster_house = cluster_house.drop(['id', 'date', 'parsed_date', 'cluster'], axis=1)

  years = cluster_house.year.unique()

  for year in years:
    year_cluster_house = cluster_house[cluster_house.year == year].copy()

    max_sqft_price = year_cluster_house["price_per_sqft"].max()

    colors = year_cluster_house.apply(lambda row: (row["price_per_sqft"] / max_sqft_price, 0, 0), axis=1)

    # plt.scatter(cluster_house["long"], cluster_house["lat"], 1, c=colors)
    # plt.hist(cluster_house["price_per_sqft"], 20)

    f, ax = plt.subplots(figsize=(16, 12))
    plt.title('Pearson Correlation')
    sns.heatmap(year_cluster_house.astype(float).corr(),linewidths=0.25,vmax=1.0, square=True, cmap="cubehelix", linecolor='k', annot=True)
    plt.yticks(rotation=0)
    plt.xticks(rotation=90)

    latMax = year_cluster_house["lat"].max()
    latMin = year_cluster_house["lat"].min()
    longMax = year_cluster_house["long"].max()
    longMin = year_cluster_house["long"].min()

    print "longMin=%.2f " % longMin + "latMin=%.2f " % latMin + "longMax=%.2f " % longMax + "latMax=%.2f " % latMax 

    plt.savefig('cluster_price_per_sqft/pearson/cluster%i_' % i  + year + ".png")
  
winsound.Beep(1000,500)
