import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
import math
import winsound
from scipy.stats import logistic
from mpl_toolkits.mplot3d import Axes3D

def price_per_sqft(row):
  return 1.0 * row["price"] / row["sqft_living"]

house = pd.read_csv("kc_house_data.csv")
house = house.sample(frac=0.01)

print len(house)

house["price_per_sqft"] = house.apply(lambda row: price_per_sqft(row), axis=1)
# house = house.query('price_per_sqft<600')



fig = plt.figure()
ax = Axes3D(fig)
ax.scatter(house["long"], house["lat"], house["price_per_sqft"])

winsound.Beep(1000,500)

plt.show()