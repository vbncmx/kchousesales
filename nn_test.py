import pandas as pd
import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
import numpy
import math
import winsound
from datetime import datetime as dt
import dateutil.parser
from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline

def getDistance(lon1, lat1, lon2, lat2):
    R = 6378137 #m    
    # https://stackoverflow.com/a/11172685/6483508
    dLon = (lon2 - lon1) * math.pi / 180
    dLat = (lat2 - lat1) * math.pi / 180
    a = math.sin(dLat/2) * math.sin(dLat/2) + math.cos(lat1 * math.pi / 180) * math.cos(lat2 * math.pi / 180) * math.sin(dLon/2) * math.sin(dLon/2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    return R * c

# define base model
def baseline_model():
	# create model
	model = Sequential()
	model.add(Dense(4, input_dim=4, kernel_initializer='normal', activation='relu'))
	model.add(Dense(12, kernel_initializer='normal', activation='relu'))
	model.add(Dense(1, kernel_initializer='normal'))
	# Compile model
	model.compile(loss='mean_squared_error', optimizer='adam')
	return model

house = pd.read_csv("ks_house_clustered_40.csv")
house["distance_from_center"] = house.apply(lambda row: getDistance(row["long"], row["lat"], -122.22, 47.63), axis=1)
house["price_per_sqft"] = house.apply(lambda row: row["price"] / row["sqft_living"], axis=1)
# house = house[house.cluster == 2].copy()


features = ["distance_from_center", "grade", "bedrooms", "bathrooms"]
X = numpy.array(house[features])
Y = numpy.array(house["price_per_sqft"])
seed = 7

# evaluate model with standardized dataset
numpy.random.seed(seed)
estimators = []
estimators.append(('standardize', StandardScaler()))
estimators.append(('mlp', KerasRegressor(build_fn=baseline_model, epochs=50, batch_size=5, verbose=0)))
pipeline = Pipeline(estimators)
kfold = KFold(n_splits=10, random_state=seed)
results = cross_val_score(pipeline, X, Y, cv=kfold)
print("Standardized: %.2f (%.2f) MSE" % (results.mean(), results.std()))

winsound.Beep(1000,500) 