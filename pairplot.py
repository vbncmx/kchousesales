import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns

house = pd.read_csv("kc_house_data.csv")
print house.head()

# Looking for nulls
print(house.isnull().any())
# Inspecting type
print(house.dtypes)

house = house.drop(['id', 'date'], axis=1)
g = sns.pairplot(house[['sqft_lot','sqft_above','price','sqft_living','bedrooms']], hue='bedrooms', palette='afmhot', size=3)
g.fig.subplots_adjust(left=0.08)
# g.fig.subplots_adjust(top=0.05)
g.set(xticklabels=[])

plt.savefig('pairplot.png')