import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
import math
import winsound
from datetime import datetime as dt
import dateutil.parser

def getDistance(lon1, lat1, lon2, lat2):
    R = 6378137 #m    
    # https://stackoverflow.com/a/11172685/6483508
    dLon = (lon2 - lon1) * math.pi / 180
    dLat = (lat2 - lat1) * math.pi / 180
    a = math.sin(dLat/2) * math.sin(dLat/2) + math.cos(lat1 * math.pi / 180) * math.cos(lat2 * math.pi / 180) * math.sin(dLon/2) * math.sin(dLon/2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    return R * c

house = pd.read_csv("kc_house_data.csv")
house['price_per_sqft'] = house.apply(lambda row: row["price"] / row["sqft_living"], axis=1)
latCenter = 47.63
lonCenter = -122.22
house['distance_from_center'] = house.apply(lambda row: getDistance(row["long"], row["lat"], lonCenter, latCenter), axis=1)
house['date_ordinal'] = house.apply(lambda row: dateutil.parser.parse(row["date"]).date().toordinal(), axis=1)
house = house.drop(['id', 'date'], axis=1)

print house.head(5)

str_list = []
for colname, colvalue in house.iteritems():
    if type(colvalue[1]) == str:
         str_list.append(colname)            
num_list = house.columns.difference(str_list)
house_num = house[num_list]
f, ax = plt.subplots(figsize=(16, 12))
plt.title('Pearson Correlation')
sns.heatmap(house_num.astype(float).corr(),linewidths=0.25,vmax=1.0, square=True, cmap="cubehelix", linecolor='k', annot=True)
plt.yticks(rotation=0)
plt.xticks(rotation=90)
plt.savefig("pearson.png")

winsound.Beep(1000,500) 