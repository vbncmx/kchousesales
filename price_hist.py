import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
import math
import winsound
from scipy.stats import logistic
from sklearn.cluster import AgglomerativeClustering
from sklearn.preprocessing import MinMaxScaler

house = pd.read_csv("ks_house_clustered_5.csv")
house = house[house.cluster == 0]
plt.hist(house["price"], 100)
winsound.Beep(1000,500)
plt.show()