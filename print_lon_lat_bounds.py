import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
import math
import winsound
from scipy.stats import logistic
from sklearn.cluster import AgglomerativeClustering
from sklearn.preprocessing import MinMaxScaler
import time

house = pd.read_csv("kc_house_data.csv")
print("lon_min = %.2f" % house["long"].min())
print("lon_max = %.2f" % house["long"].max())
print("lat_min = %.2f" % house["lat"].min())
print("lat_max = %.2f" % house["lat"].max())
