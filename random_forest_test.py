import pandas as pd
import numpy
import winsound
from sklearn.ensemble import ExtraTreesRegressor
from sklearn.model_selection import cross_val_score
from sklearn.metrics import r2_score
import math
from matplotlib import pyplot as plt

def squaredDif(x1, x2):
	return (x1 - x2) * (x1 - x2)

def squaredLogDif(x1, x2):
	logDif = math.log(x1 + 1) - math.log(x2 + 1)
	return logDif * logDif 

rmses = []
n_cluster_range = range(1, 11)
for n_clusters in n_cluster_range:

	house = pd.read_csv("clustered_csv/kc_clustered_%i.csv" % n_clusters)

	RMSLE_avg = 0
	RMSE_avg = 0
	test_size = 0
	for cl in range(1, n_clusters + 1):

		MSE_cluster_avg = 0
		MSLE_cluster_avg = 0
		n_tries = 20
		house_clustered = house[house.dominate_cluster==cl].copy()

		for i in range(0, n_tries):

			RMSLE_sum = 0
			RMSE_sum = 0

			house_shuffled = house_clustered.sample(frac=1)
			house_len = len(house_shuffled.index)

			house_train = house_shuffled.head(int(9 * house_len / 10)).copy()
			house_test = house_shuffled.tail(house_len - len(house_train.index)).copy()

			features = ["long", "lat", "grade", "bedrooms", "bathrooms", "sqft_living", "sqft_lot", "waterfront", "sqft_above", "zipcode"]
			X_train = numpy.array(house_train[features])
			Y_train = numpy.array(house_train["price"])

			# regr = RandomForestRegressor(min_samples_leaf=6, n_estimators = 10, random_state=0)
			regr = ExtraTreesRegressor(max_depth=14, n_estimators = 10, random_state=0)
			regr.fit(X_train, Y_train)
			Y_pred_train = regr.predict(X_train)

			#remove outliers
			indeces = list(sorted(range(0, len(Y_train)), key=lambda i: squaredDif(Y_pred_train[i], Y_train[i])))
			indeces = indeces[: int(9 * len(indeces) / 10)]
			X_train_outlied = X_train[indeces, :]  #numpy.array(map(lambda i: X_train[:, i] , indeces))
			Y_train_outlied = Y_train[indeces] #numpy.array(map(lambda i: Y_train[i] , indeces))
			regr.fit(X_train_outlied, Y_train_outlied)

			X_test = numpy.array(house_test[features])
			Y_test = numpy.array(house_test["price"])
			Y_pred_test = regr.predict(X_test)
			Y_pred_train = regr.predict(X_train)		

			MSLE_cluster_avg += sum(map(lambda i: squaredLogDif(Y_pred_test[i], Y_test[i]), range(0, len(Y_pred_test))))
			MSE_cluster_avg += sum(map(lambda i: squaredDif(Y_pred_test[i], Y_test[i]), range(0, len(Y_pred_test))))

		test_size += len(Y_pred_test)
		RMSE_avg += MSE_cluster_avg / n_tries
		RMSLE_avg += MSLE_cluster_avg / n_tries
	
	RMSLE_avg = math.sqrt(RMSLE_avg / test_size)
	RMSE_avg = math.sqrt(RMSE_avg / test_size)

	print("N = %i: RMSLE_avg = %.2f, exp(RMSLE_avg) = %.2f, RMSE_avg = %.2f" % (n_clusters, RMSLE_avg, math.exp(RMSLE_avg), RMSE_avg))

	rmses.append(RMSE_avg)

plt.plot(n_cluster_range, rmses)
plt.show()
