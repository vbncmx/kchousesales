import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
import math
import winsound
from scipy.stats import logistic
from mpl_toolkits.mplot3d import Axes3D
from dateutil.parser import *
from datetime import *

def getDistance(lon1, lat1, lon2, lat2):
    R = 6378137 #m    
    # https://stackoverflow.com/a/11172685/6483508
    dLon = (lon2 - lon1) * math.pi / 180
    dLat = (lat2 - lat1) * math.pi / 180
    a = math.sin(dLat/2) * math.sin(dLat/2) + math.cos(lat1 * math.pi / 180) * math.cos(lat2 * math.pi / 180) * math.sin(dLon/2) * math.sin(dLon/2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    return R * c

def getYear(row):
  return int(row["date"][:4])

def getMonth(row):
  return int(row["date"][4:6])

def getDensity(row, house_set):
  row_date = parse(row["date"])
  month_before = row_date - timedelta(days=30)
  month_after = row_date + timedelta(days=30)
  same_period_house = house_set[(house_set.parsed_date > month_before) & (house_set.parsed_date < month_after)]

  lon = row["long"]
  lat = row["lat"]
  s = same_period_house.apply(lambda x: getDistance(x["long"], x["lat"], lon, lat), axis=1)
  s = s[s < 1000]

  return s.size

def price_per_sqft(row):
  return 1.0 * row["price"] / row["sqft_living"]

house = pd.read_csv("kc_house_data.csv")
house["parsed_date"] = house.apply(lambda row: parse(row["date"]), axis=1)
house["density"] = house.apply(lambda row: getDensity(row, house), axis=1)

house.to_csv('ks_house_data_with_density.csv')

winsound.Beep(1000,500)
