import requests
import cv2 as cv
import numpy as np

def cropGoogleImage(img):
    img_hsv=cv.cvtColor(img, cv.COLOR_BGR2HSV)
    lower = np.array([1,1,1])
    upper = np.array([4,255,255])
    thresh = cv.inRange(img_hsv, lower, upper)
    img2, contours, hierarchy = cv.findContours(thresh,cv.RETR_TREE,cv.CHAIN_APPROX_SIMPLE)

    rects = map(lambda c: cv.boundingRect(c), contours)
    rects = sorted(rects, key=lambda r: r[2] * r[3], reverse=True)
    rects = rects[:2]
    rects = sorted(rects, key=lambda r: r[0])
    top_left = rects[0]
    bottom_right = rects[1]

    x_min = int(top_left[0] + top_left[2] / 2)
    y_min = top_left[1] + top_left[3]
    x_max = int(bottom_right[0] + bottom_right[2] / 2)
    y_max = bottom_right[1] + bottom_right[3]

    print([x_min, x_max, y_min, y_max])

    return img[y_min:y_max, x_min:x_max]

def saveGoogleImage(latTopLeft, longTopLeft, latBottomRight, longBottomRight, path):
    top_left = "%.2f,%.2f" % (latTopLeft, longTopLeft)
    bottom_right = "%.2f,%.2f" % (latBottomRight, longBottomRight)

    command = "http://maps.googleapis.com/maps/api/staticmap?sensor=false&size=640x480&visible=" \
    + top_left \
    + "&visible=" \
    + bottom_right \
    + "&markers=color:red%7Ccolor:red%7Clabel:A%7C" \
    + top_left \
    + "&markers=color:red%7Ccolor:red%7Clabel:B%7C" \
    + bottom_right
    
    f=open(path,'wb')
    f.write(requests.get(command).content)
    f.close()

    img = cv.imread(path)
    img = cropGoogleImage(img)
    cv.imwrite(path, img)
    cv.imshow(path, img)
    cv.waitKey(0)

saveGoogleImage(47.78, -122.52, 47.16, -121.31, 'google_image.png')


